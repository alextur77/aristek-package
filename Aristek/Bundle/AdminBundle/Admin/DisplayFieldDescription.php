<?php

namespace Aristek\Bundle\AdminBundle\Admin;

use Sonata\DoctrineORMAdminBundle\Admin\FieldDescription;

/**
 * Class DisplayFieldDescription
 */
class DisplayFieldDescription extends FieldDescription
{
    /**
     * Constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldValue($object, $fieldName)
    {
        $data = $this->getOption('data');

        return $data ?: parent::getFieldValue($object, $fieldName);
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        parent::setType('string');
    }

}
