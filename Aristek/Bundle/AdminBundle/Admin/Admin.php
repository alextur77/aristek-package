<?php

namespace Aristek\Bundle\AdminBundle\Admin;

use Aristek\Component\Util\Pluralization;
use Gedmo\Sluggable\Util\Urlizer;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class Admin
 */
class Admin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        $this->class = $class;
        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @return \Sonata\AdminBundle\Translator\LabelTranslatorStrategyInterface
     */
    public function getLabelTranslatorStrategy()
    {
        return $this->configurationPool->getContainer()->get('aristek.admin.label_translator_strategy');
    }

    /**
     * @return string
     */
    public function getBaseRouteName()
    {
        if (!$this->baseRouteName) {
            preg_match(self::CLASS_REGEX, $this->class, $matches);

            if (!$matches) {
                throw new \RuntimeException(sprintf('Cannot automatically determine base route name, please define a default `baseRouteName` value for the admin class `%s`', get_class($this)));
            }

            if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
                $this->baseRouteName = sprintf('%s_%s',
                                               $this->getParent()->getBaseRouteName(),
                                               $this->urlize($matches[5])
                );
            } else {
                $this->baseRouteName = sprintf('%s%s',
                                               empty($matches[1]) ? '' : $this->urlize($matches[1]).'_',
                                               Pluralization::pluralize($this->urlize($matches[5], '_'))
                );
            }
        }

        return $this->baseRouteName;
    }

    /**
     * Returns the baseRoutePattern used to generate the routing information
     *
     * @throws \RuntimeException
     *
     * @return string the baseRoutePattern used to generate the routing information
     */
    public function getBaseRoutePattern()
    {
        if (!$this->baseRoutePattern) {
            preg_match(self::CLASS_REGEX, $this->class, $matches);

            if (!$matches) {
                throw new \RuntimeException(sprintf('Please define a default `baseRoutePattern` value for the admin class `%s`', get_class($this)));
            }

            if ($this->isChild()) { // the admin class is a child, prefix it with the parent route name
                $this->baseRoutePattern = sprintf('%s/{id}/%s',
                                                  $this->getParent()->getBaseRoutePattern(),
                                                  $this->urlize($matches[5], '-')
                );
            } else {
                $this->baseRoutePattern = sprintf('/%s%s',
                                                  empty($matches[1]) ? '' : $this->urlize($matches[1], '-').'/',
                                                  Pluralization::pluralize($this->urlize($matches[5], '-'))
                );
            }
        }

        return $this->baseRoutePattern;
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return $this->configurationPool->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param string $word
     * @param string $sep
     *
     * @return string
     */
    public function urlize($word, $sep = '_')
    {
        $word = preg_replace('/(?<=[a-z])([A-Z])/', '_$1', $word);

        return Urlizer::urlize($word, $sep);
    }

    /**
     * @return null|AdminInterface|Admin
     */
    public function getParent()
    {
        return parent::getParent();
    }

    /**
     * Generates the breadcrumbs array.
     *
     * Note: the method will be called by the top admin instance (parent => child)
     *
     * @param string             $action
     * @param ItemInterface|null $menu
     *
     * @return ItemInterface
     */
    public function buildBreadcrumbs($action, MenuItemInterface $menu = null)
    {
        return parent::buildBreadcrumbs($action, $menu);
    }

    /**
     * Adds a flash message for type.
     *
     * @param string $type
     * @param string $message
     * @param array  $parameters
     */
    protected function sendMessage($type, $message, $parameters = [])
    {
        /** @var Session $session */
        $session = $this->getRequest()->getSession();
        $session->getFlashBag()->add($type, $this->trans($message, $parameters));
    }
}
