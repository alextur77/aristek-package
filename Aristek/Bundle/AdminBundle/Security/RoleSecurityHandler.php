<?php

namespace Aristek\Bundle\AdminBundle\Security;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Security\Handler\RoleSecurityHandler as BaseRoleSecurityHandler;

/**
 * Class RoleSecurityHandler
 */
class RoleSecurityHandler extends BaseRoleSecurityHandler
{
    /**
     * @param AdminInterface $admin
     * @param array|string   $attributes
     * @param null           $object
     *
     * @return bool
     * @throws \Exception
     */
    public function isGranted(AdminInterface $admin, $attributes, $object = null)
    {
        return parent::isGranted($admin, 'FULL', $object) || parent::isGranted($admin, $attributes, $object);
    }
}
