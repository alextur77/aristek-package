<?php

namespace Aristek\Bundle\AdminBundle\Translator;

use Sonata\AdminBundle\Translator\LabelTranslatorStrategyInterface;

/**
 * Class UnderscoreLabelTranslatorStrategy
 */
class UnderscoreLabelTranslatorStrategy implements LabelTranslatorStrategyInterface
{
    /**
     * {@inheritDoc}
     */
    public function getLabel($label, $context = '', $type = '')
    {
        if ($label == 'dashboard' && $context == 'breadcrumb' && $type == 'link') {
            return 'breadcrumb.link_dashboard';
        }
        $label = str_replace('.', '_', $label);

        return sprintf('%s.%s', $type, strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', $label)));
    }
}
