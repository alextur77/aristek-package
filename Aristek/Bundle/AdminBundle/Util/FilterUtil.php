<?php

namespace Aristek\Bundle\AdminBundle\Util;

use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;

/**
 * Class Filter
 */
class FilterUtil
{
    public static function process($filter)
    {
        $filter['value'] = trim($filter['value']);
        switch ($filter['type']) {
            case ChoiceType::TYPE_NOT_CONTAINS:
                $cond = 'NOT LIKE';
                $value = '%' . $filter['value'] . '%';
                break;
            case ChoiceType::TYPE_EQUAL:
                $cond = '=';
                $value = $filter['value'];
                break;
            case ChoiceType::TYPE_CONTAINS:
            default:
                $cond = 'LIKE';
                $value = '%' . $filter['value'] . '%';
        }

        return [$cond, $value];
    }
}
