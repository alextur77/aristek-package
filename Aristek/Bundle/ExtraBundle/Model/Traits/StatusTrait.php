<?php

namespace Aristek\Bundle\ExtraBundle\Model\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait StatusTrait
 */
trait StatusTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $active = true;

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * get active status
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active status
     */
    public function setActive()
    {
        $this->active = true;
    }

    /**
     * Set inactive status
     */
    public function setInactive()
    {
        $this->active = false;
    }
}

