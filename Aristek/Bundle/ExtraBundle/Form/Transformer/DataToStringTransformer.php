<?php

namespace Aristek\Bundle\ExtraBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class DataToStringTransformer
 */
class DataToStringTransformer implements DataTransformerInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @param array $options
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * @param mixed $value
     *
     * @return mixed|string
     */
    public function transform($value)
    {

        if (null == $value) {
            return '';
        } else if ($value instanceof \DateTime) {
            $value = $value->format('m/d/Y');
        } else if (is_object($value)) {
            /** @noinspection PhpUndefinedMethodInspection */
            $value =  $value->__toString();
        } else if (is_null($value)) {
            $value = '';
        } else if (is_array($value)) {
            $value = print_r($value, true);
        } else if (isset($this->options['choices']) && !empty($this->options['choices'][$value])) {
            $value = $this->options['choices'][$value];
            $value = $value ?: '';
        } else if (isset($this->options['type'])) {
            if ('money' === $this->options['type']) {
                $value = '$' . number_format($value, 2, '.', ',');
            }
        }

        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function reverseTransform($value)
    {
        return $value;
    }
}
