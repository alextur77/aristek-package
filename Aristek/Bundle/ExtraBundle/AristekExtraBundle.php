<?php

namespace Aristek\Bundle\ExtraBundle;

use Aristek\Bundle\ExtraBundle\DependencyInjection\Compiler\LoggerAwareCompilerPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AristekExtraBundle
 */
class AristekExtraBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new LoggerAwareCompilerPass(), PassConfig::TYPE_AFTER_REMOVING);
    }
}
