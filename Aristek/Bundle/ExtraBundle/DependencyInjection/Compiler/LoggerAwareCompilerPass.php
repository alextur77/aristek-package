<?php

namespace Aristek\Bundle\ExtraBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class LoggerAwareCompilerPass
 */
class LoggerAwareCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        $services = $container->getDefinitions();
        foreach ($services as $service) {
            if (is_subclass_of($service->getClass(), 'Psr\Log\LoggerAwareInterface')) {
                $service->addMethodCall('setLogger', [$container->getDefinition('logger')]);
            }
        }
    }
}

