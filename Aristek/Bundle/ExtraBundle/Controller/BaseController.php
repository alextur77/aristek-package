<?php

namespace Aristek\Bundle\ExtraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BaseController
 */
class BaseController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
     */
    public function getFlashBag()
    {
        /** @noinspection PhpUndefinedMethodInspection */

        return $this->get('request')->getSession()->getFlashBag();
    }

    /**
     * @param string $route
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectTo($route)
    {
        return $this->redirect($this->generateUrl($route));
    }

    /**
     * Generates CSRF
     *
     * @return null|string
     */
    protected function getCsrf()
    {
        return $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;
    }
}
