<?php

namespace Aristek\Bundle\ExtraBundle\ORM\Mapping;

use Aristek\Component\Util\Pluralization;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;

/**
 * Class UnderscorePluralNamingStrategy
 */
class UnderscorePluralNamingStrategy extends UnderscoreNamingStrategy
{
    /**
     * @param string $className
     *
     * @return string
     */
    public function classToTableName($className)
    {
        $name = parent::classToTableName($className);
        $name = Pluralization::pluralize($name);

        return $name;
    }
}

