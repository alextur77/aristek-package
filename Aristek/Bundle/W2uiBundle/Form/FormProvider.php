<?php

namespace Aristek\Bundle\W2uiBundle\Form;

use Symfony\Component\Form\Form;

/**
 * Class FormProvider
 */
class FormProvider
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var array|callable
     */
    protected $data;

    /**
     * @var callable[]
     */
    protected $fieldHandlers = [];

    /**
     * @var object
     */
    protected $entity;

    /**
     * @param Form $form
     */
    public function __construct($form = null)
    {
        $this->form = $form;
    }

    /**
     * @param callable|null $callback callback(int $limit, int $offset, array $filters)
     *
     * @return $this|array
     */
    public function getData($callback = null)
    {
        if (null !== $callback) {
            $this->data = $callback;

            return $this;
        }

        if (is_callable($this->data)) {
            return call_user_func($this->data, $this->entity);
        }

        $data = [];
        if ($this->form instanceof Form) {
            $form = $this->form->setData($this->entity);

            $formView = $form->createView();

            foreach ($formView->children as $field) {
                $value = $field->vars['data'];
                if (array_key_exists($field->vars['name'], $this->fieldHandlers)) {
                    $value = call_user_func($this->fieldHandlers[$field->vars['name']], $value);
                }
                $data[$field->vars['full_name']] = $value;
            }
        }

        return $data;
    }

    /**
     * @param string   $field
     * @param callable $callback
     */
    public function addFieldHandler($field, $callback)
    {
        $this->fieldHandlers[$field] = $callback;
    }

    /**
     * @param object $entity
     *
     * @return FormProvider
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }
}

