<?php

namespace Aristek\Bundle\W2uiBundle\Controller;

use Aristek\Bundle\ExtraBundle\Controller\BaseController;
use Aristek\Bundle\W2uiBundle\Form\FormProvider;
use Aristek\Bundle\W2uiBundle\Grid\DataGridProvider;
use Aristek\Bundle\W2uiBundle\Util\W2uiCommand;
use Aristek\Component\ORM\AbstractEntityManager;
use Aristek\Component\ORM\EntityRepository;
use Doctrine\Common\CommonException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class W2uiController
 */
abstract class W2uiController extends BaseController
{

    /**
     * @return string
     */
    protected abstract function getName();

    /**
     * @return string
     */
    protected abstract function getTitle();

    /**
     * @return Response
     *
     * @Route()
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('w2index.html.twig', ['name' => $this->getName(), 'title' => $this->getTitle()]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route()
     * @Method("POST")
     */
    public function processAction(Request $request)
    {
        switch ($request->get('cmd')) {
            case W2uiCommand::COMMAND_GET_RECORDS:
                $response = $this->processRecordsRequest($request);
                break;
            case W2uiCommand::COMMAND_DELETE_RECORDS:
                $response = $this->processDeleteRequest($request);
                break;
            case W2uiCommand::COMMAND_GET_RECORD:
                $response = $this->processRecordRequest($request);
                break;
            case W2uiCommand::COMMAND_SAVE_RECORD:
                $response = $this->processSaveRequest($request);
                break;
            default:
                $response = new Response('Command not found');
        }

        return $response;
    }

    /**
     * @param int $id
     *
     * @return object
     */
    protected function getEntity($id)
    {
        $user = $this->getRepository()->find($id);
        if (null === $user) {
            $user = $this->getManager()->create();
        }

        return $user;
    }

    /**
     * @param object $entity
     */
    protected function saveEntity($entity)
    {
        $this->getManager()->save($entity);
    }

    /**
     * @param int[] $ids
     *
     * @throws \Exception
     */
    protected function doDelete($ids)
    {
        $this->getManager()->remove($ids);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws CommonException
     */
    protected function processRecordsRequest(Request $request)
    {
        $provider = $this->getDataGrid();
        $provider->setLimit($request->get('limit'));
        $provider->setOffset($request->get('offset'));
        $provider->setFilters($request->get('search'));
        $provider->setSorts($request->get('sort'));

        return $this->get('aristek_w2ui.w2ui_json_response')->getListResponse(
            $provider->getData(),
            $provider->getCount()
        );
    }

    /**
     * @return DataGridProvider
     */
    protected function getDataGrid()
    {
        throw new \LogicException('Method doDelete not implemented!');
    }

    /**
     * @return Response
     *
     * @Route("/form")
     */
    public function formAction()
    {
        $form = $this->getForm();

        return $this->render('w2form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @return Form
     */
    protected function getForm()
    {
        throw new \LogicException('Method getForm not implemented!');
    }

    /**
     * @return FormProvider
     */
    protected function getFormData()
    {
        throw new \LogicException('Method getForm not implemented!');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    protected function processDeleteRequest(Request $request)
    {
        $ids = (array) $request->get('selected');
        try {
            $this->doDelete($ids);
            $response = $this->get('aristek_w2ui.w2ui_json_response')->getSuccessResponse();
        } catch (\Exception $e) {
            $response = $this->get('aristek_w2ui.w2ui_json_response')->getErrorResponse('Could not delete users');
        }

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    protected function processRecordRequest(Request $request)
    {
        $form = $this->getFormData();
        $form->setEntity($this->getEntity($request->get('recid', 0)));

        return $this->get('aristek_w2ui.w2ui_json_response')->getRecordResponse($form->getData());
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    protected function processSaveRequest($request)
    {
        $record = $request->request->get('record');
        $request->request->remove('record');
        $post = [];
        foreach ($record as $field => $value) {
            $field = str_replace('[]', '', $field);

            $field = rtrim($field, ']');
            $parts = explode('[', $field);
            if (0 === count($parts)) {
                $request->request->add($field);
                continue;
            }
            $post[$parts[0]][$parts[1]] = $value;
        }
        if (count($post) > 0) {
            $request->request->add($post);
        }

        $entity = $this->getEntity($request->get('recid', 0));
        if (null === $entity) {
            throw new \LogicException('Entity can not be null!');
        }

        $form = $this->getForm()->setData($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->saveEntity($entity);
        }

        return $this->get('aristek_w2ui.w2ui_json_response')->getSuccessResponse();
    }

    /**
     * @return EntityRepository
     */
    protected function getRepository()
    {
        throw new \LogicException('Method getRepository not implemented!');
    }

    /**
     * @return AbstractEntityManager
     */
    protected function getManager()
    {
        throw new \LogicException('Method getManager not implemented!');
    }
}

