<?php

namespace Aristek\Bundle\W2uiBundle\Grid;

use Aristek\Component\ORM\AbstractEntityFilter;
use Aristek\Component\ORM\WhereFilter;

/**
 * Class DataGridProvider
 */
class DataGridProvider
{
    /**
     * @var int|callable
     */
    protected $count;

    /**
     * @var array|callable
     */
    protected $data;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var array
     */
    protected $sorts;

    /**
     * @var array
     */
    protected $filters;

    /**
     * @var AbstractEntityFilter
     */
    private $entityFilter;

    /**
     * @var bool
     */
    private $isFilterProcessed = false;

    /**
     * @var callable
     */
    private $postLoad = null;

    /**
     * @param AbstractEntityFilter $entityFilter
     */
    public function __construct($entityFilter = null)
    {

        $this->entityFilter = $entityFilter;
    }

    /**
     * @param callable|null $callback callback(int $limit, int $offset, array $filters)
     *
     * @return $this|array
     */
    public function getData($callback = null)
    {
        if (null !== $callback) {
            $this->data = $callback;

            return $this;
        }

        if (is_callable($this->data)) {
            return call_user_func($this->data, $this->limit, $this->offset, $this->filters);
        }

        if ($this->data) {
            return $this->data;
        }

        if (null !== $this->entityFilter) {
            return $this->getEntityResult();
        }

        return empty($this->data) ? [] : $this->data;
    }

    /**
     * @param callable|null $callback callback(array $filters)
     *
     * @return $this|int
     */
    public function getCount($callback = null)
    {
        if (null !== $callback) {
            $this->count = $callback;

            return $this;
        }

        if (is_callable($this->count)) {
            return call_user_func($this->count, $this->filters);
        }

        if (null !== $this->entityFilter) {
            $filter = clone $this->entityFilter;
            $this->applyFilters($filter);

            return $filter->getCount()->getResult();
        }

        return $this->count;
    }

    /**
     * @param array $sorts
     *
     * @return DataGridProvider
     */
    public function setSorts($sorts)
    {
        $this->sorts = $sorts;

        return $this;
    }

    /**
     * @param AbstractEntityFilter $entityFilter
     */
    protected function applyFilters($entityFilter)
    {
        if ($this->isFilterProcessed || 0 === count($this->filters)) {
            return;
        }

        foreach ($this->filters as $filter) {
            switch ($filter['operator']) {
                case 'begins':
                    $type = WhereFilter::TYPE_BEGIN_WITH;
                    break;
                case 'contains':
                    $type = WhereFilter::TYPE_CONTAINS;
                    break;
                case 'ends':
                    $type = WhereFilter::TYPE_CONTAINS;
                    break;
                default:
                    $type = WhereFilter::TYPE_IS;
            }
            call_user_func(array($entityFilter, 'set' . ucfirst($filter['field'])), $filter['value'], $type);
        }

        $this->isFilterProcessed = true;
    }

    /**
     * @param AbstractEntityFilter $entityFilter
     */
    protected function applySorts($entityFilter)
    {
        if (null === $this->sorts) {
            return;
        }

        $orders = [];
        foreach ($this->sorts as $sort) {
            $orders[$sort['field']] = $sort['direction'];
        }

        if (count($orders) > 0) {
            $entityFilter->setOrder($orders);
        }
    }

    /**
     * @param int $limit
     *
     * @return DataGridProvider
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param int $offset
     *
     * @return DataGridProvider
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @param array $filters
     *
     * @return DataGridProvider
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @param callable $callback
     *
     * @return $this
     */
    public function postLoad($callback)
    {
        $this->postLoad = $callback;

        return $this;
    }

    /**
     * @return array
     */
    protected function getEntityResult()
    {
        $filter = clone $this->entityFilter;
        $this->applyFilters($filter);
        $this->applySorts($filter);

        if ($this->limit) {
            $filter->setLimit($this->limit);
        }

        if ($this->offset) {
            $filter->setOffset($this->offset);
        }

        $results = $filter->getResult();

        if ($this->postLoad) {
            $results = call_user_func($this->postLoad, $results);
        }

        return $results;
    }
}

