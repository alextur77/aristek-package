<?php

namespace Aristek\Bundle\W2uiBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class W2uiJsonResponse
 */
class W2uiJsonResponse
{
    /**
     * @param array $data
     * @param int   $count
     *
     * @return JsonResponse
     */
    public function getListResponse(array $data, $count = null)
    {
        // add recid if needs
        if (isset($data[0]) && !isset($data[0]['recid'])) {
            foreach ($data as &$row) {
                $row['recid'] = $row['id'];
            }
        }

        $res = [
            'status' => 'success',
            'total' => $count,
            'records' => $data
        ];

        return new JsonResponse($res);
    }

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    public function getRecordResponse(array $data)
    {
        $res = [
            'status' => 'success',
            'record' => $data
        ];

        return new JsonResponse($res);
    }

    /**
     * @return JsonResponse
     */
    public function getSuccessResponse()
    {
        $res = [
            'status' => 'success',
        ];

        return new JsonResponse($res);
    }

    /**
     * @param string $message
     *
     * @return JsonResponse
     */
    public function getErrorResponse($message = '')
    {
        $res = [
            'status' => 'success',
            'message' => $message
        ];

        return new JsonResponse($res);
    }
}

