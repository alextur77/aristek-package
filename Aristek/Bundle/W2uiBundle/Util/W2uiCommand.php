<?php

namespace Aristek\Bundle\W2uiBundle\Util;

/**
 * Class W2uiCommand
 */
class W2uiCommand
{
    const COMMAND_GET_RECORDS = 'get-records';
    const COMMAND_DELETE_RECORDS = 'delete-records';
    const COMMAND_GET_RECORD = 'get-record';
    const COMMAND_SAVE_RECORD = 'save-record';
}

