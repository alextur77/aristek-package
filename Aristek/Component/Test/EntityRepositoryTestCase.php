<?php

namespace Aristek\Component\Test;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class EntityRepositoryTestCase
 */
abstract class EntityRepositoryTestCase extends KernelTestCase
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @return string
     */
    protected abstract function getRepositoryName();

    /**
     * sets up
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();

        $this->repository = $this->em->getRepository($this->getRepositoryName());

        parent::setUp();
    }
}

