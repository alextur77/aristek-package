<?php

namespace Aristek\Component\DependencyInjection\Loader;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as BaseYamlFileLoader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class YamlFileLoader
 */
class YamlFileLoader
{
    /**
     * @var array
     */
    protected $paths = [];

    /**
     * @var BaseYamlFileLoader
     */
    private $loader;

    /**
     * @param ContainerBuilder                  $container
     * @param FileLocatorInterface|array|string $paths
     */
    public function __construct(ContainerBuilder $container, $paths)
    {
        if ($paths instanceof FileLocatorInterface) {
            $locator = $paths;
        } else {
            $this->paths = (array) $paths;
            $locator = new FileLocator($paths);
        }
        if (0 === count($this->paths)) {
            throw new \InvalidArgumentException('No paths found. You should provide paths to the constructor');
        }
        $this->loader = new BaseYamlFileLoader($container, $locator);
    }

    /**
     * Loads single config
     *
     * @param string $file
     */
    public function load($file)
    {
        $this->loader->load($file);
    }

    /**
     * Loads all configs
     */
    public function loadAll()
    {
        $finder = new Finder();
        $finder->files()->in($this->paths)->name('*.yml');
        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $this->loader->load($file->getFilename());
        }
    }
}
