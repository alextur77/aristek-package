<?php

namespace Aristek\Component\ORM;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class AbstractEntityManager
 */
abstract class AbstractEntityManager implements LoggerAwareInterface
{
    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @param EntityRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
        $this->em = $repository->em();
    }

    /**
     * @return object
     */
    public function create()
    {
        $class = $this->repository->getClassName();

        return new $class;
    }

    /**
     * @param object $entity
     * @param bool   $flush
     */
    public function save($entity, $flush = true)
    {
        $this->em->persist($entity);
        if ($flush) {
            $this->em->flush($entity);
        }
    }

    /**
     * @param object|integer|object[]|integer[] $entity
     * @param bool                              $flush
     *
     * @throws \Exception
     */
    public function remove($entity, $flush = true)
    {
        $entities = (array) $entity;

        if (0 == count($entities)) {
            return;
        }

        if (!is_object($entities[0])) {
            $entities = $this->repository->findBy(['id' => $entities]);
        }

        if ($flush) {
            $this->em->beginTransaction();
        }
        try {
            foreach ($entities as $entity) {
                $this->em->remove($entity);
            }

            if ($flush) {
                $this->em->flush();
                $this->em->commit();
            }
        } catch (\Exception $e) {
            if ($flush) {
                $this->em->rollback();
            }
            $this->logger->error($e->getMessage());
            throw new \Exception('Could not delete records');
        }
    }
}

