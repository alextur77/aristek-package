<?php

namespace Aristek\Component\ORM;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

/**
 * Class WhereFilter
 */
class WhereFilter
{
    const TYPE_IS = 'is';
    const TYPE_BEGIN_WITH = 'begin';
    const TYPE_END_WITH = 'end';
    const TYPE_CONTAINS = 'contains';

    protected $field;

    protected $value;

    protected $type;

    /**
     * @param string $field
     * @param mixed  $value
     * @param string $type
     */
    public function __construct($field, $value, $type = self::TYPE_IS)
    {
        $this->field = $field;
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return Expr\Comparison
     */
    public function getWhere($queryBuilder)
    {
        $exp = new Expr();
        $field = $this->field;
        if (false === strpos($field, '.')) {
            $field = $this->getRootAlias($queryBuilder) . '.' . $field;
        }
        $param = $this->convertFieldToParam($field);
        switch ($this->type) {
            case self::TYPE_BEGIN_WITH:
                $where = $exp->like($field, ":$param");
                $queryBuilder->setParameter($param, $this->value . '%');
                break;
            case self::TYPE_END_WITH:
                $where = $exp->like($field, ":$param");
                $queryBuilder->setParameter($param, '%'. $this->value);
                break;
            case self::TYPE_CONTAINS:
                $where = $exp->like($field, ":$param");
                $queryBuilder->setParameter($param, '%' . $this->value . '%');
                break;
            default:
                $where = $exp->like($field, ":$param");
                $queryBuilder->setParameter($param, $this->value);
        }

        return $where;
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return string
     */
    public function getRootAlias($queryBuilder)
    {
        $aliases = $queryBuilder->getRootAliases();

        return $aliases[0];
    }

    /**
     * @param string $field
     *
     * @return string
     */
    private function convertFieldToParam($field)
    {
        return str_replace('.', '_', $field);
    }
}

