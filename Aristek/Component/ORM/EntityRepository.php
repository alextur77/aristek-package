<?php

namespace Aristek\Component\ORM;

use Doctrine\ORM\EntityRepository as DoctrineEntityRepository;

/**
 * Class EntityRepository
 */
class EntityRepository extends DoctrineEntityRepository
{
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function em()
    {
        return $this->_em;
    }
}

