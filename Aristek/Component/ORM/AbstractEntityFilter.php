<?php

namespace Aristek\Component\ORM;

use Doctrine\Common\CommonException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * Class AbstractEntityFilter
 */
abstract class AbstractEntityFilter
{
    const WHERE_AND = 'AND';
    const WHERE_OR = 'OR';

    protected $whereType = self::WHERE_AND;

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var int
     */
    protected $hydrationMode = Query::HYDRATE_OBJECT;

    /**
     * @var WhereFilter[]
     */
    protected $filters = [];

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        $this->buildWhere();

        return $this->queryBuilder->getQuery()->getResult($this->hydrationMode);
    }

    protected function buildWhere()
    {
        $wheres = [];
        foreach ($this->filters as $filter) {
            if ($filter instanceof WhereFilter) {
                $wheres[] = $filter->getWhere($this->queryBuilder);
            } else {
                $wheres[] = $filter;
            }

        }

        if (0 === count($wheres)) {
            return;
        }

        if ($this->whereType == AbstractEntityFilter::WHERE_AND) {
            $where = call_user_func_array(array($this->queryBuilder->expr(), 'andX'), $wheres);
        } else {
            $where = call_user_func_array(array($this->queryBuilder->expr(), 'orX'), $wheres);
        }

        $this->queryBuilder->andWhere($where);
    }

    /**
     * @param array|string $select
     *
     * @return $this
     *
     * @throws CommonException
     */
    public function setSelect($select)
    {
        $select = (array) $select;

        $this->queryBuilder->resetDQLPart('select');
        $aliases = $this->queryBuilder->getRootAliases();
        foreach ($select as $field) {
            if (false === strpos($field, '.')) {
                $this->queryBuilder->addSelect($aliases[0] . '.' . $field);
            } else {
                $parts = explode('.', $field);
                if (!in_array($parts[0], $aliases)) {
                    throw new CommonException('Alias not found.');
                }

                $this->queryBuilder->addSelect($field);
            }
        }

        return $this;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function setLimit($limit)
    {
        if (null === $limit) {
            return $this;
        }

        $this->queryBuilder->setMaxResults($limit);

        return $this;
    }

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function setOffset($offset)
    {
        if (null === $offset) {
            return $this;
        }

        $this->queryBuilder->setFirstResult($offset);

        return $this;
    }

    /**
     * @param array $orders
     */
    public function setOrder(array $orders)
    {
        foreach ($orders as $key => $value) {
            $this->queryBuilder->addOrderBy($this->getFieldName($key), $value);
        }
    }

    /**
     * @return $this
     */
    public function getCount()
    {
        $this->queryBuilder->resetDQLPart('select');
        $this->queryBuilder->setFirstResult(null);
        $this->queryBuilder->setMaxResults(null);
        $aliases = $this->queryBuilder->getRootAliases();
        $this->queryBuilder->addSelect(sprintf('COUNT(%s)', $aliases[0] . '.id'));
        $this->hydrationMode = Query::HYDRATE_SINGLE_SCALAR;

        return $this;
    }

    /**
     * @param mixed $hydrationMode
     *
     * @return $this
     */
    public function setHydrationMode($hydrationMode)
    {
        $this->hydrationMode = $hydrationMode;

        return $this;
    }

    /**
     * @param string $field
     *
     * @return string
     */
    public function getFieldName($field)
    {
        if (false === strpos($field, '.')) {
            return $this->getRootAlias() . '.' . $field;
        }

        return $field;
    }

    /**
     * @return string
     */
    public function getRootAlias()
    {
        $aliases = $this->queryBuilder->getRootAliases();

        return $aliases[0];
    }
}

